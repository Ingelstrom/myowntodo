package com.boomtownmobileapps.myowntodo;

import android.support.v4.app.Fragment;

/**
 * Created by henrikingelstrom on 2017-03-13.
 */

public class TaskListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new TaskListFragment();
    }
}
